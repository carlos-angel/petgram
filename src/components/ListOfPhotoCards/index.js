import React from 'react';
import PropTypes from 'prop-types';
import { PhotoCard } from '../PhotoCard';

import { List } from './styles';

export const ListOfPhotoCardsComponent = ({ data: { photos = [] } } = {}) => {
  return (
    <List>
      {photos.map((photo) => (
        <PhotoCard key={photo.id} {...photo} />
      ))}
    </List>
  );
};

ListOfPhotoCardsComponent.propTypes = {
  data: PropTypes.shape({
    photos: PropTypes.array,
  }),
};
