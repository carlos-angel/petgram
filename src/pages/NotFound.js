import React from 'react';
import { Link } from '@reach/router';

export const NotFound = () => {
  return (
    <>
      <h1>Esta página no existe</h1>
      <p>
        Regresar al <Link to='/'>Home</Link>
      </p>
    </>
  );
};
