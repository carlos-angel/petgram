import React from 'react';
import { Helmet } from 'react-helmet';

import { ListOfCategories } from '../components/ListOfCategories';
import { ListOfPhotoCards } from '../container/ListOfPhotoCards';

const HomePage = ({ id }) => {
  return (
    <>
      <Helmet>
        <title>Petgram - Tu app de fotos de máscotas</title>
        <meta
          name='description'
          content='Con Petgram puedes encontrar fotos de animales domésticos muy bonitos.'
        />
      </Helmet>
      <ListOfCategories />
      <ListOfPhotoCards categoryId={id} />
    </>
  );
};

export const Home = React.memo(HomePage, (prevPros, props) => {
  return prevPros.id === props.id;
});
