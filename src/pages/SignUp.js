import React, { useContext } from 'react';
import { Link } from '@reach/router';

import { Context } from '../Context';
import { Layout } from '../components/Layout';
import { RegisterMutation } from '../container/RegisterMutation';
import { UserForm } from '../components/UserForm';

export const SignUp = () => {
  const { activateAuth } = useContext(Context);

  return (
    <Layout title='Registrase'>
      <RegisterMutation>
        {(register, { loading, error }) => {
          const onSubmit = ({ email, password }) => {
            const input = { email, password };
            const variables = { input };

            register({ variables }).then(({ data }) =>
              activateAuth(data.signup),
            );
          };

          const errorMsg = error && 'El usuario ya existe o hay algún problema';

          return (
            <>
              <UserForm
                error={errorMsg}
                disabled={loading}
                onSubmit={onSubmit}
                title='Registrarse'
              />
              <p>
                Already have an account? <Link to='/login'>Log in</Link>
              </p>
            </>
          );
        }}
      </RegisterMutation>
    </Layout>
  );
};
