import React, { useContext } from 'react';
import { Link } from '@reach/router';

import { UserForm } from '../components/UserForm';
import { Context } from '../Context';
import { LoginMutation } from '../container/LoginMutation';
import { Layout } from '../components/Layout';

export const Login = () => {
  const { activateAuth } = useContext(Context);

  return (
    <Layout title='Inicio de sesión'>
      <LoginMutation>
        {(login, { loading, error }) => {
          const onSubmit = ({ email, password }) => {
            const input = { email, password };
            const variables = { input };

            login({ variables }).then(({ data }) => activateAuth(data.login));
          };

          const errorMsg = error && 'Email y/o contraseña incorrecta(s).';

          return (
            <>
              <UserForm
                error={errorMsg}
                disabled={loading}
                onSubmit={onSubmit}
                title='Iniciar sesión'
              />
              <p>
                Don't have an account? <Link to='/signup'>Sign Up</Link>
              </p>
            </>
          );
        }}
      </LoginMutation>
    </Layout>
  );
};
