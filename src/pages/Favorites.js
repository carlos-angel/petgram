import React from 'react';

import { FavoritesWithQuery } from '../container/GetFavorites';
import { Layout } from '../components/Layout';

export default () => {
  return (
    <Layout
      title='Mis favoritos'
      subtitle='Aquí puedes encontrar tus fotos de animales favoritos'
    >
      <FavoritesWithQuery />
    </Layout>
  );
};
