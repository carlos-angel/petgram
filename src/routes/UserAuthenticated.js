import React from 'react';
import { Router, Redirect } from '@reach/router';

import { Home } from '../pages/Home';
import { NotFound } from '../pages/NotFound';

const Favorites = React.lazy(() => import('../pages/Favorites'));
const User = React.lazy(() => import('../pages/User'));
const Detail = React.lazy(() => import('../pages/Detail'));

export default function UserAuthenticated({ isAuth }) {
  return (
    <Router>
      <NotFound default />
      <Home path='/' />
      <Home path='/pet/:id' />
      <Detail path='/detail/:detailId' />
      <Favorites path='/favorites' />
      <User path='/user' />
      {isAuth && <Redirect noThrow from='/login' to='/' />}
      {isAuth && <Redirect noThrow from='/signup' to='/' />}
    </Router>
  );
}
